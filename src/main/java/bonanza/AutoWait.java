package bonanza;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;

import static org.opencv.core.Core.mean;

public class AutoWait {
    private final int mAutoReelEnd;
    private final int mAutoReelStart;
    private final int mAutoRowStart;
    private final int mAutoRowEnd;

    private final int mStartRowStart = 549;
    private final int mStartRowEnd;
    private final int mStartReelStart = 762;
    private final int mStartReelEnd;

    private final double[] mfMeanAutoColor = {1.1244444444444444, 199.62, 11.434444444444445};
    private final double mfMaxRelDiff;


    private int mCount;
    private Mat mScreenMat;
    private boolean mFree;

    public Mat getScreenMat() {
        return mScreenMat;
    }

    public AutoWait(int iAutoRowStart, int iAutoRowEnd, int iAutoReelStart, int iAutoReelEnd) {
        mAutoRowStart = iAutoRowStart;
        mAutoRowEnd = iAutoRowEnd;
        mAutoReelStart = iAutoReelStart;
        mAutoReelEnd = iAutoReelEnd;
        mCount = 0;
        mfMaxRelDiff = 3.;
        mStartRowEnd = mStartRowStart + 60;
        mStartReelEnd = mStartReelStart + 128;
    }

    public boolean CheckButton() {
        ++mCount;
        mScreenMat = ScreenUtil.getScreenBufferedImage( );
        Mat mat = mScreenMat.submat(mAutoRowStart, mAutoRowEnd, mAutoReelStart, mAutoReelEnd);
        Scalar sc = mean(mat);
        boolean res = true;
        for (int idx = 0; idx < 3; ++idx) {
            res = Math.abs(sc.val[idx] - mfMeanAutoColor[idx]) * mfMaxRelDiff < mfMeanAutoColor[idx];
            if (!res)
                break;
        }

        return res;
    }

    public int count() {
        return mCount;
    }

    private void CheckFreeFinished() {
         do {
            UserUtil.sleep(200);
        } while (!CheckButton( ));
        mCount = 0;
        mFree = true;
    }

    public boolean CheckFreeGame() {
        Mat mat = mScreenMat.submat(mStartRowStart, mStartRowEnd, mStartReelStart, mStartReelEnd);
        Scalar mean = Core.mean(mat);
        Scalar cald = new Scalar(90, 180, 250);
        double diff = ScreenUtil.compare(mean, cald);
        boolean res = diff < 20.;
        if (res) {
            Imgcodecs.imwrite("WORK\\startRed.jpg", mScreenMat);
            Bonanza.mDump.writeLn("Start Free");
            PressFreeStart( );
            UserUtil.sleep(2000);
            CheckFreeFinished( );
            UserUtil.sleep(2);
        }
        return res;
    }

    private void PressFreeStart() {
        ScreenUtil.click(mStartReelStart, mStartRowStart);
    }

    public boolean isFree() {
        return mFree;
    }
}
