package bonanza;

import org.opencv.core.Mat;

public class Comparison {

    public static double getDifference(Mat iMg1, Mat iMg2) throws IllegalArgumentException {
        int width = iMg1.cols( );
        int height = iMg1.rows( );
        int width2 = iMg2.cols( );
        int height2 = iMg2.rows( );
        if (width != width2 || height != height2) {
            throw new IllegalArgumentException(String.format("Images must have the same dimensions: (%d,%d) vs. (%d,%d)", width, height, width2, height2));
        }

        double diff = 0L;
        for (int y = 0; y < width; y++) {
            for (int x = 0; x < height; x++) {
                diff += pixelDiff(iMg1.get(x, y), iMg2.get(x, y));
            }
        }
        double maxDiff = 3. * 255 * width * height;

        return 1. * diff / maxDiff;
    }

    private static double pixelDiff(double[] iRGB1, double[] iRGB2) {
        double res = 0.;
        for (int idx = 0; idx < 3; ++idx)
            res += Math.abs(iRGB1[idx] - iRGB2[idx]);

        return res;
    }


}
