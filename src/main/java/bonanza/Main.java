package bonanza;

import org.opencv.core.Core;

public class Main {

    /* Compulsory */
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        Bonanza dyn = new Bonanza( );
        dyn.execute( );
    }

}
