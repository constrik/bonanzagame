package biblio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextWriter {
    BufferedWriter mBufWriter = null;

    public TextWriter(String iFName, boolean iCreate) {
        try {
            mBufWriter = iCreate ? new BufferedWriter(new FileWriter(new File(iFName))) : null;
        } catch (Exception e) {
            e.printStackTrace( );
        }
    }

    public TextWriter(String iFName) {
        this(iFName, true);
    }

    public void flush() {
        try {
            if (mBufWriter != null)
                mBufWriter.flush( );
        } catch (IOException e) {
            e.printStackTrace( );
        }
    }

    public void writeLn(String iStr) {
        write(iStr + "\n");
    }

    public void write(String iStr) {
        try {
            if (mBufWriter != null)
                mBufWriter.write(iStr);
        } catch (IOException e) {
            e.printStackTrace( );
        }
    }

}


