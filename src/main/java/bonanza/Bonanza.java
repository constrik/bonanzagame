package bonanza;

import biblio.TextWriter;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.*;

public class Bonanza {
    private final static boolean mfSaveImages = false;
    private final static boolean mfSaveScreen = false;
    private final static boolean mfDoDump = true;

    private final static String mfBalance = "Balance";
    private final static String mfWin = "Win";

    private final static int mfReelsNo = 6;
    private final static int mfRowStart = 2;
    private final static int mfRowEnd = 7;

    private final static int mfPictRowStart = 301;
    private final static int mfPictRowEnd = mfPictRowStart + 479;
    private final static int mfPictReelStart = 508;
    private final static int mfPictReelEnd = mfPictReelStart + 903;

    private final static int mfDeltaX = 12;
    private final static int mfDeltaY = 12;

    private final static int mfAutoRowStart = 654;
    private final static int mfAutoRowEnd = mfAutoRowStart + 30;
    private final static int mfAutoReelStart = 1544;
    private final static int mfAutoReelEnd = mfAutoReelStart + 30;

    private final static int mfBalanceRowStart = 821;
    private final static int mfBalanceRowEnd = mfBalanceRowStart + 44;
    private final static int mfBalanceReelStart = 174;
    private final static int mfBalanceReelEnd = mfBalanceReelStart + 371;

    private final static int mfWinRowStart = 821;
    private final static int mfWinRowEnd = mfWinRowStart + 44;
    private final static int mfWinReelStart = 1542;
    private final static int mfWinReelEnd = mfWinReelStart + 295;

    private final static int mfAutoIdx = 35;
    private final static int mfBet = 60;

    private final static char[] mfSymbolLetter = {'C', 'R', 'B', 'G', 'A', 'K', 'Q', 'J', 'T', 'N',};

    private boolean mQuit;
    private int mBalance;
    private int mPrevBalance;
    private int mWin;
    private Mat mMatFull;
    private Mat mMatField;
    private Mat mMatReel;
    private Mat mMatRow;
    private int mScreenNo;
    public static TextWriter mDump;
    private TextWriter mSizeData;

    private Map<Integer, Set<CModel>> mRowModelM = new HashMap<>( );
    private boolean mFree;

    Bonanza() {
        Init( );
    }

    private void Init() {
        UserUtil.makePath("WORK");
        mDump = new TextWriter("WORK//Dump.txt", mfDoDump);
        mSizeData = new TextWriter("WORK//SizeData.txt");
        ScreenUtil.init();
    }

    public void execute() {
        mQuit = false;
        RestoreImages( );
        mScreenNo = 0;
        while (RenewBrowser( )) {
            if (waitForAutoButtonFirstTime( )) {
                do {
                    clickExec( );
                    waitForAutoButton(true);
                    getNumberValue( );
                    analyzeScreen( );
                    ++mScreenNo;
                } while (!mQuit);
            }
        }
    }

    private boolean RenewBrowser() {
        mSizeData.writeLn("Try restart browser");
        ScreenUtil.goToAddress("https://www.vegasslotsonline.com/big-time-gaming/bonanza/");
        UserUtil.sleep(8000);
        ScreenUtil.click(814, 618); // click Start
        UserUtil.sleep(3000);
        ScreenUtil.click(1225, 350); //click Full Screen
        UserUtil.sleep(10000);
        ScreenUtil.click(500, 500); // click Continue
        return true;
    }

    private boolean waitForAutoButtonFirstTime() {
        boolean res = waitForAutoButton(false);
        if (res) {
            getNumberValue( );
        }
        return res;
    }

    private void getNumberValue() {
        mPrevBalance = mBalance;
        mBalance = ScreenUtil.getNumberValue(mMatFull, mfBalanceRowStart, mfBalanceRowEnd, mfBalanceReelStart, mfBalanceReelEnd, mfBalance);
        mWin = ScreenUtil.getNumberValue(mMatFull, mfWinRowStart, mfWinRowEnd, mfWinReelStart, mfWinReelEnd, mfWin);
        System.out.println("" + mBalance + "\t" + mWin);
    }

    private void analyzeScreen() {
        mMatField = mMatFull.submat(mfPictRowStart, mfPictRowEnd, mfPictReelStart, mfPictReelEnd);
        if (mfSaveImages)
            SaveImages( );
        if (mfSaveScreen)
            SaveScreen( );
        RestoreDivision( );
        //    AnalyzeWin( );
    }

    private void clickExec() {
        ScreenUtil.click(1618, 672);
    }

    private boolean waitForAutoButton(boolean iCheckFree) {
        mQuit = false;
        mFree = false;
        AutoWait autoWait = new AutoWait(mfAutoRowStart, mfAutoRowEnd, mfAutoReelStart, mfAutoReelEnd);
        boolean res;
        boolean over;
        do {
            res = autoWait.CheckButton( );
            if (!res && iCheckFree)
                res = autoWait.CheckFreeGame( );
            UserUtil.sleep(50);
            over = autoWait.count( ) >= mfAutoIdx;
        }
        while (!res && !over);
        mQuit = over;
        if (res) {
            mMatFull = autoWait.getScreenMat( );
            Imgcodecs.imwrite("WORK\\screen" + mScreenNo + ".jpg", mMatFull);
            mFree = autoWait.isFree( );
        }
        return !mQuit;
    }

    private void RestoreImages() {
        for (int row = mfRowStart; row <= mfRowEnd; ++row) {
            Set<CModel> modelS = new HashSet<>( );
            for (char ch : mfSymbolLetter) {
                String fName = "" + ch + row + ".png";
                String name = "MODEL//" + fName;
                Mat mat = Imgcodecs.imread(name);
                mat = ScreenUtil.Equalize(mat);
                CModel model = new CModel(fName);
                model.setMat(mat);
                modelS.add(model);
                fName = "" + ch + row + "eq.png";
                name = "MODEL//" + fName;
                Imgcodecs.imwrite(name, mat);
            }
            mRowModelM.put(row, modelS);
        }
    }

    private void RestoreDivision() {
        List<Integer> reelLenL = new ArrayList<>( );
        mDump.writeLn("Screen\t" + mScreenNo);
        for (int reel = 0; reel < mfReelsNo; ++reel) {
            mDump.writeLn("Reel\t" + reel);
            getReelMat(reel);
            Pic pic = null;
            for (int row = mfRowStart; row <= mfRowEnd; ++row) {
                //               WriteLnDump("\tTestRow\t" + row);
                Pic picRow = getPIC(row);
                if (pic == null || pic.getmDiff( ) > picRow.getmDiff( ))
                    pic = picRow;
            }
            mDump.writeLn("Pic\t " + pic.getmStr( ) + "\t" + pic.getmModelRow( ) + "\t" + pic.getmDiff( ));
            reelLenL.add(pic.getmStr( ).charAt(1) - '0');
        }
        if (mFree) {
            int balance = mBalance - mWin;
            int win = balance - mPrevBalance + mfBet;
            PrintData(null, balance, win, false);
        }

        PrintData(reelLenL, mBalance, mWin, mFree);

        mDump.flush( );
        mSizeData.flush( );
    }

    private void PrintData(List<Integer> iLenL, int iValBal, int iValWin, boolean iFree) {
        char chType = iFree ? 'F' : 'R';
        mSizeData.write(chType + "\t" + mScreenNo + "\t" + iValBal + "\t" + iValWin);
        if (iLenL != null)
            for (Integer intg : iLenL) mSizeData.write("\t" + intg);
        mSizeData.writeLn("");
    }

    private void getReelMat(int iReel) {
        int startX = (int) Math.round(iReel * mMatField.cols( ) / (double) mfReelsNo);
        int endX = startX + (int) Math.round(mMatField.cols( ) / (double) mfReelsNo);
        endX = Math.min(endX, mMatField.cols( ));
        mMatReel = mMatField.submat(0, mMatField.rows( ), startX, endX);
        Imgcodecs.imwrite("WORK//Reel" + iReel + ".png", mMatReel);
    }

    private void getRowMat(int iRow, int iMaxRow) {
        int startY = (int) Math.round(iRow * mMatReel.rows( ) / (double) iMaxRow);
        int endY = startY + (int) Math.round(mMatReel.rows( ) / (double) iMaxRow);
        mMatRow = mMatReel.submat(startY + mfDeltaY, endY - mfDeltaY, mfDeltaX, mMatReel.cols( ) - mfDeltaX);
        Imgcodecs.imwrite("WORK//Row" + iRow + ".png", mMatRow);
        mMatRow = ScreenUtil.Equalize(mMatRow);
        Imgcodecs.imwrite("WORK//RowEq" + iRow + ".png", mMatRow);
    }


    private Pic getPIC(int iRow) {
        double modelDiff = 1.;
        String modelName = null;
        int modelRow = -1;
        for (int row = 0; row < iRow; ++row) {
            //WriteLnDump("\t\tRow\t" + row);
            Set<CModel> modelSet = mRowModelM.get(iRow);
            getRowMat(row, iRow);
            double diffRow = 1.;
            String modelNameRow = null;
            for (CModel model : modelSet) {
                double delta = Comparison.getDifference(model.getMat( ), mMatRow);
                if (delta < diffRow) {
                    diffRow = delta;
                    modelNameRow = model.getName( );
                }
            }
            //  WriteLnDump("\t\tpic\t" + modelNameRow+"\t"+row+"\t"+diffRow);
            if (diffRow < modelDiff) {
                modelDiff = diffRow;
                modelName = modelNameRow;
                modelRow = row;
            }
        }
        assert modelName != null;
        char ch = modelName.charAt(0);
        return new Pic(ch, iRow, modelRow, modelDiff);
    }


    private void SaveImages() {
        UserUtil.makePath("SAMPLE");
        Mat mat = mMatFull.submat(mfAutoRowStart, mfAutoRowEnd, mfAutoReelStart, mfAutoReelEnd);
        Imgcodecs.imwrite("SAMPLE//auto.png", mat);
    }

    private void SaveScreen() {
        Imgcodecs.imwrite(String.format("WORK/Screen%d.png", mScreenNo), mMatFull);
    }
}

