package bonanza;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UserUtil {
    public static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace( );
        }
    }

    public static void makePath(String iPath) {
        Path pathP = Paths.get(iPath);
        if (Files.notExists(pathP)) {
            File pathF = new File(iPath);
            if (!pathF.mkdir( )) {
                System.out.println("Can not crate dir");
                System.exit(-8);
            }
        }
    }
}

