package bonanza;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.opencv.imgproc.Imgproc.*;

public class ScreenUtil {
    public static Robot msBot;

    public static void init() {
        try {
            msBot = new Robot(MouseInfo.getPointerInfo( ).getDevice( ));
            msBot.setAutoDelay(50);
            msBot.setAutoWaitForIdle(true);
        } catch (Exception e) {
            e.printStackTrace( );
            System.exit(-5);
        }
    }

    public static Mat getScreenBufferedImage() {
        BufferedImage bi = null;
        try {
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit( ).getScreenSize( ));
            bi = msBot.createScreenCapture(screenRect);
        } catch (Exception ex) {
            ex.printStackTrace( );
            System.exit(-4);
        }
        return ScreenUtil.bufferedImageToMat(bi);
    }

    private static Mat bufferedImageToMat(BufferedImage image) {
        Mat mat = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream( );
            ImageIO.write(image, "png", byteArrayOutputStream);
            byteArrayOutputStream.flush( );
            mat = Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray( )), Imgcodecs.IMREAD_UNCHANGED);
        } catch (Exception ex) {
            ex.printStackTrace( );
            System.exit(-7);
        }

        return mat;
    }

    public static void click(int x, int y) {
        try {
            msBot.mouseMove(x, y);
            UserUtil.sleep(500);
            msBot.mouseMove(x, y);
            UserUtil.sleep(500);

            msBot.mousePress(InputEvent.BUTTON1_MASK);
            UserUtil.sleep(1000);
            msBot.mouseRelease(InputEvent.BUTTON1_MASK);
        } catch (Exception ex) {
            ex.printStackTrace( );
            System.exit(-6);
        }

    }

    public static Mat Equalize(Mat iMat) {
        Mat ycrcb = new Mat( );
        cvtColor(iMat, ycrcb, COLOR_BGR2YCrCb);
        List<Mat> channels = new ArrayList<>( );
        Core.split(ycrcb, channels);
        Imgproc.equalizeHist(channels.get(0), channels.get(0));
        Mat middle = new Mat( );
        Mat result = new Mat( );
        Core.merge(channels, ycrcb);
        cvtColor(ycrcb, middle, COLOR_YCrCb2BGR);
        resize(middle, result, new Size(8, 8), 0, 0, INTER_LANCZOS4);

        return result;
    }

    public static double compare(Scalar iScalar1, Scalar iScalar2) {
        return Math.abs(iScalar1.val[0] - iScalar2.val[0]) + Math.abs(iScalar1.val[1] - iScalar2.val[1]) + Math.abs(iScalar1.val[2] - iScalar2.val[2]);
    }

    public static void OCRNegative(Mat iMat, String iName) throws IOException, InterruptedException {
        Core.bitwise_not(iMat, iMat);
        Imgcodecs.imwrite("SAMPLE\\" + iName + ".png", iMat);

        Process p = Runtime.getRuntime( ).exec("C:\\Users\\constantin.razinsky\\AppData\\Local" +
                "\\Tesseract-OCR\\tesseract.exe SAMPLE\\" + iName + ".png" + " SAMPLE\\" + iName);
        p.waitFor( );
    }

    public static int getNumberValue(Mat iMatFull, int iRowStart, int iRowEnd, int iReelStart, int iReelEnd, String iName) {
        try {
            Mat mat = iMatFull.submat(iRowStart, iRowEnd, iReelStart, iReelEnd);
            OCRNegative(mat, iName);
        } catch (Exception ex) {
            ex.printStackTrace( );
            System.out.println("OCR error");
            System.exit(-1);
        }
        return ScreenUtil.getOCRValue("SAMPLE\\" + iName + ".txt");
    }

    public static int getOCRValue(String iStr) {
        int res = -1;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(iStr));
            String line = reader.readLine( );
            res = getIntFromString(line);
        } catch (IOException e) {
            e.printStackTrace( );
        }

        return res;
    }

    private static int getIntFromString(String iStr) {
        int res = 0;
        for (int idx = 0; idx < iStr.length( ); ++idx) {
            char ch = iStr.charAt(idx);
            if (Character.isDigit(ch)) {
                res *= 10;
                res += (ch - '0');
            }
        }
        return res;
    }
    
    public static void goToAddress(String iAddr) {

        try {
            Desktop.getDesktop( ).browse(new URL(iAddr).toURI( ));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
