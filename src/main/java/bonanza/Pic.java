package bonanza;

public class Pic {
    private final int mModelRow;
    private final double mDiff;
    private final String mStr;

    public int getmModelRow() {
        return mModelRow;
    }

    public double getmDiff() {
        return mDiff;
    }

    public String getmStr() {
        return mStr;
    }


    public Pic(char iCh, int iRow, int iModelRow, double iModelDiff) {
        mStr = String.valueOf(iCh) + iRow;
        mDiff = iModelDiff;
        mModelRow = iModelRow;
    }

}
