package bonanza;

import org.opencv.core.Mat;

public class CModel {
    private final String mName;
    private Mat mMat;

    CModel(String iName) {
        mName = iName;
    }

    public String getName() {
        return mName;
    }

    public void setMat(Mat iMat) {
        mMat = iMat;
     }

    public Mat getMat() {
        return mMat;
    }

}