call mvn clean install -Dmaven.test.skip
IF /I "%ERRORLEVEL%" NEQ "0" (
	pause > nul
	exit /B 1
)
call mvn dependency:copy-dependencies
copy target\dependency\*.jar %ADMIN_CONSOLE_HOME%\runtimes\4.0
pause


